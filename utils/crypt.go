package utils

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"time"
)

func Encrypt(text string) string {

	key := []byte("fsilva-apiS3cr3t")
	h := hmac.New(sha256.New, key)
	h.Write([]byte(text))

	encPass := base64.StdEncoding.EncodeToString(h.Sum(nil))

	return encPass
}

func GenerateSessionToken(email, password string) string {
	return Encrypt(string(time.Now().Unix()) + ":" + email + ":" + password)
}
