package main

import (
	"fsilva-api/routes"
	"log"
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"
)

func main() {

	router := httprouter.New()
	port := os.Getenv("PORT")

	if port == "" {
		port = "9000"
	}

	router.POST("/sign_in", routes.NewSignInRoute().Run)
	router.POST("/google_sign_in", routes.NewGoogleSignInRoute().Run)

	router.POST("/sign_up", routes.NewSignUpRoute().Run)

	router.GET("/profile", routes.NewGetProfileRoute().Run)
	router.PUT("/profile", routes.NewEditProfileRoute().Run)

	router.POST("/request_password_reset", routes.NewRequestPasswordResetRoute().Run)

	router.POST("/reset_password", routes.NewRequestPasswordResetRoute().Run)

	router.POST("/sign_out", routes.NewSignOutRoute().Run)

	log.Fatal(http.ListenAndServe(":"+port, router))
}
