package db

import (
	"fmt"
	"fsilva-api/utils"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/steinbacher/goose"
)

var db *sqlx.DB

func init() {

	connString := os.Getenv("DATABASE_URL")
	if connString == "" {
		connString = "root:password@/fsilva"
	}

	dbConn, err := sqlx.Open("mysql", connString)
	if err != nil {
		fmt.Println(err)
	}
	db = dbConn

	db.MapperFunc(utils.ToSnake)

}
