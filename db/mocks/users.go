package mocks

import (
	"fsilva-api/structs"
)

type MockUsersRepository struct {
	GetIdByEmailAndPasswordWasCalled       bool
	GetIdByEmailAndPasswordReturnResults   bool
	GetIdByEmailFromGoogleWasCalled        bool
	GetIdByEmailFromGoogleReturnResults    bool
	StoreSessionTokenWasCalled             bool
	CreateGoogleSignupWasCalled            bool
	CreateUserWasCalled                    bool
	CreateUserDuplicateEmail               bool
	GetProfileBySessionTokenWasCalled      bool
	GetProfileBySessionTokenReturnResults  bool
	UpdateProfileBySessionTokenWasCalled   bool
	GetIdBySessionTokenWasCalled           bool
	GetIdBySessionTokenReturnResults       bool
	StorePasswordResetTokenWasCalled       bool
	GetIdByEmailWasCalled                  bool
	GetIdByEmailReturnResults              bool
	GetIdByResetPasswordTokenWasCalled     bool
	GetIdByResetPasswordTokenReturnResults bool
	UpdatePasswordWasCalled                bool
	UpdatePasswordReturnResults            bool
	DeleteSessionWasCalled                 bool
}

func (repo *MockUsersRepository) GetIdByEmailAndPassword(email, password string) int {
	repo.GetIdByEmailAndPasswordWasCalled = true

	if repo.GetIdByEmailAndPasswordReturnResults {
		return 1
	} else {
		return 0
	}
}

func (repo *MockUsersRepository) GetIdByEmailFromGoogle(email string) int {
	repo.GetIdByEmailFromGoogleWasCalled = true

	if repo.GetIdByEmailFromGoogleReturnResults {
		return 1
	} else {
		return 0
	}
}

func (repo *MockUsersRepository) StoreSessionToken(id int, token string) error {
	repo.StoreSessionTokenWasCalled = true
	return nil
}

func (repo *MockUsersRepository) CreateGoogleSignup(email string) (int, error) {
	repo.CreateGoogleSignupWasCalled = true
	return 1, nil
}

func (repo *MockUsersRepository) CreateUser(email, password string) int {
	repo.CreateUserWasCalled = true

	result := 1

	if repo.CreateUserDuplicateEmail {
		result = 0
	}

	return result
}

func (repo *MockUsersRepository) GetProfileBySessionToken(token string) structs.Profile {
	repo.GetProfileBySessionTokenWasCalled = true

	profile := structs.Profile{}

	if repo.GetProfileBySessionTokenReturnResults {
		profile.Email = "a@a.com"
	}

	return profile

}

func (repo *MockUsersRepository) UpdateProfileBySessionToken(token, email, name, address, tel string) error {
	repo.CreateGoogleSignupWasCalled = true
	return nil
}

func (repo *MockUsersRepository) GetIdBySessionToken(token string) int {
	repo.GetIdBySessionTokenWasCalled = true

	if repo.GetIdBySessionTokenReturnResults {
		return 1
	} else {
		return 0
	}
}

func (repo *MockUsersRepository) StorePasswordResetToken(id int, token string) error {
	repo.StorePasswordResetTokenWasCalled = true
	return nil
}

func (repo *MockUsersRepository) GetIdByEmail(email string) int {
	repo.GetIdByEmailWasCalled = true
	if repo.GetIdByEmailReturnResults {
		return 1
	} else {
		return 0
	}
}

func (repo *MockUsersRepository) GetIdByPasswordResetToken(token string) int {
	repo.GetIdByResetPasswordTokenWasCalled = true
	if repo.GetIdByResetPasswordTokenReturnResults {
		return 1
	} else {
		return 0
	}
}

func (repo *MockUsersRepository) UpdatePassword(id int, password string) {
	repo.UpdatePasswordWasCalled = true
}

func (repo *MockUsersRepository) DeleteSessionToken(token string) {
	repo.DeleteSessionWasCalled = true
}
