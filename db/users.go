package db

import (
	"fsilva-api/structs"
	"log"
)

type IUsersRepository interface {
	GetIdByEmailAndPassword(string, string) int
	StoreSessionToken(int, string) error
	GetIdByEmailFromGoogle(string) int
	CreateGoogleSignup(string) (int, error)
	CreateUser(string, string) int
	GetProfileBySessionToken(string) structs.Profile
	UpdateProfileBySessionToken(string, string, string, string, string) error
	GetIdBySessionToken(string) int
	StorePasswordResetToken(int, string) error
	GetIdByEmail(string) int
	GetIdByPasswordResetToken(string) int
	UpdatePassword(int, string)
	DeleteSessionToken(string)
}
type UsersRepository struct{}

func NewUsersRepository() UsersRepository {
	return UsersRepository{}
}

func (repo UsersRepository) GetIdByEmailAndPassword(email, password string) int {
	id := 0
	db.Get(&id, "SELECT id FROM users WHERE email = ? AND password = ? LIMIT 1", email, password)
	return id
}

func (repo UsersRepository) StoreSessionToken(id int, token string) error {
	insert := `UPDATE users SET session_token = ? WHERE id = ?`
	db.MustExec(insert, token, id)
	return nil
}

func (repo UsersRepository) GetIdByEmailFromGoogle(email string) int {
	id := 0
	db.Get(&id, "SELECT id FROM users WHERE email = ? AND from_google = true LIMIT 1", email)
	return id
}

func (repo UsersRepository) CreateGoogleSignup(email string) (int, error) {

	insert := `INSERT INTO users (email, from_google) VALUES(?, 1)`
	res, err := db.Exec(insert, email)

	if err != nil {
		return 0, err
	}

	id, _ := res.LastInsertId()

	return int(id), err
}

func (repo UsersRepository) CreateUser(email, password string) int {

	insert := `INSERT INTO users (email, password) VALUES(?, ?)`
	res, err := db.Exec(insert, email, password)

	if err != nil {
		return 0
	}

	id, _ := res.LastInsertId()

	return int(id)
}

func (repo UsersRepository) GetProfileBySessionToken(token string) structs.Profile {
	var profile structs.Profile
	db.QueryRowx("SELECT email, name, address, tel FROM users WHERE session_token = ? LIMIT 1", token).StructScan(&profile)
	return profile
}

func (repo UsersRepository) UpdateProfileBySessionToken(token, email, name, address, tel string) error {
	insert := `UPDATE users SET email = ?, name = ?, address = ?, tel = ? WHERE session_token = ?`
	_, err := db.Exec(insert, email, name, address, tel, token)

	if err != nil {
		log.Print(err.Error())
		return err
	}

	return nil
}

func (repo UsersRepository) GetIdBySessionToken(token string) int {
	id := 0
	db.Get(&id, "SELECT id FROM users WHERE session_token = ? LIMIT 1", token)
	return id
}

func (repo UsersRepository) StorePasswordResetToken(id int, token string) error {
	insert := `UPDATE users SET password_reset_token = ? WHERE id = ?`
	db.MustExec(insert, token, id)
	return nil
}

func (repo UsersRepository) GetIdByEmail(email string) int {
	id := 0
	db.Get(&id, "SELECT id FROM users WHERE email = ? LIMIT 1", email)
	return id
}

func (repo UsersRepository) GetIdByPasswordResetToken(token string) int {
	id := 0
	db.Get(&id, "SELECT id FROM users WHERE password_reset_token = ? LIMIT 1", token)
	return id
}

func (repo UsersRepository) UpdatePassword(id int, password string) {
	insert := `UPDATE users SET password = ? WHERE id = ?`
	db.MustExec(insert, password, id)
}

func (repo UsersRepository) DeleteSessionToken(token string) {
	insert := `UPDATE users SET session_token = NULL WHERE session_token = ?`
	db.MustExec(insert, token)
}
