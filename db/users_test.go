package db

import (
	"fsilva-api/utils"
	"strings"
	"testing"
)

func TestGetIdByEmailAndPasswordExists(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()
	insert := `INSERT INTO users (email, password) VALUES (?, ?)`

	email := "a@a.com"
	password := utils.Encrypt("aaaaaaaa")

	db.MustExec(insert, email, password)

	result := repo.GetIdByEmailAndPassword(email, password)

	if result == 0 {
		t.Error("Email and password should exist")
	}

}

func TestCheckEmailAndPasswordDoesntExists(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()
	insert := `INSERT INTO users (email, password) VALUES (?, ?)`

	email := "a@a.com"
	password := utils.Encrypt("aaaaaaaa")

	db.MustExec(insert, email, password)

	result := repo.GetIdByEmailAndPassword("ww", "aa")

	if result != 0 {
		t.Error("Email and password should not exist")
	}

}

func TestStoreSessionToken(t *testing.T) {
	clearDB()

	repo := NewUsersRepository()
	insert := `INSERT INTO users (email, password) VALUES (?, ?)`
	email := "a@a.com"
	password := utils.Encrypt("aaaaaaaa")

	res := db.MustExec(insert, email, password)

	id, _ := res.LastInsertId()

	repo.StoreSessionToken(int(id), "a")

	result := ""
	db.Get(&result, "SELECT session_token FROM users WHERE email = ? AND password = ? LIMIT 1", email, password)

	if result == "" {
		t.Error("Token should be stored")
	}

}

func TestGetIdByEmailFromGoogleExists(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()
	insert := `INSERT INTO users (email, from_google) VALUES (?, true)`

	email := "a@a.com"

	db.MustExec(insert, email)

	result := repo.GetIdByEmailFromGoogle(email)

	if result == 0 {
		t.Error("Email should exist")
	}

}

func TestCreateGoogleSignup(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()
	//insert := `INSERT INTO users (email, password) VALUES (?, ?)`
	//db.MustExec(insert, email, password)

	email := "a@a.com"

	id, err := repo.CreateGoogleSignup(email)

	if err != nil {
		t.Fatal(err)
	}

	result := 0
	db.Get(&result, "SELECT id FROM users WHERE email = ? AND from_google = 1 LIMIT 1", email)

	if result != id {
		t.Error("User should exist")
	}

}

func TestCreateGoogleSignupEmailExists(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()

	email := "a@a.com"
	insert := `INSERT INTO users (email) VALUES (?)`
	db.MustExec(insert, email)

	_, err := repo.CreateGoogleSignup(email)

	if !strings.Contains(err.Error(), "Duplicate") {
		t.Error("Email is not duplicate")
	}

}

func TestCreateUser(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()
	//insert := `INSERT INTO users (email, password) VALUES (?, ?)`
	//db.MustExec(insert, email, password)

	email := "a@a.com"
	password := "aa"

	id := repo.CreateUser(email, password)

	result := 0
	db.Get(&result, "SELECT id FROM users ORDER BY ID desc LIMIT 1")

	if result != id {
		t.Error("User should exist")
	}

}

func TestCreateUserEmailExists(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()

	email := "a@a.com"
	password := "aa"

	insert := `INSERT INTO users (email, password) VALUES (?, ?)`
	db.MustExec(insert, email, password)

	id := repo.CreateUser(email, password)

	if id != 0 {
		t.Error("Email is not duplicate")
	}

}

func TestGetProfileBySessionToken(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()
	insert := `INSERT INTO users (email, password, address, name, tel, session_token) VALUES (?, ?, ?, ?, ?, ?)`

	email := "a@a.com"
	name := "Fabian"
	address := "Marco Bruto"
	tel := "111"
	token := "token"
	password := utils.Encrypt("aaaaaaaa")

	db.MustExec(insert, email, password, address, name, tel, token)

	result := repo.GetProfileBySessionToken(token)

	if result.Email == "" {
		t.Error("Profile should be populated")
	}

}

func TestGetIdBySessionTokenExists(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()
	insert := `INSERT INTO users (session_token) VALUES (?)`

	token := "token"

	db.MustExec(insert, token)

	result := repo.GetIdBySessionToken(token)

	if result == 0 {
		t.Error("Email should exist")
	}

}

func TestStorePasswordResetToken(t *testing.T) {
	clearDB()

	repo := NewUsersRepository()
	insert := `INSERT INTO users (email, password) VALUES (?, ?)`
	email := "a@a.com"
	password := utils.Encrypt("aaaaaaaa")

	res := db.MustExec(insert, email, password)

	id, _ := res.LastInsertId()

	repo.StorePasswordResetToken(int(id), "a")

	result := ""
	db.Get(&result, "SELECT password_reset_token FROM users WHERE id = ? LIMIT 1", id)

	if result == "" {
		t.Error("Token should be stored")
	}

}

func TestGetIdByEmail(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()

	email := "a@a.com"

	insert := `INSERT INTO users (email) VALUES (?)`

	db.MustExec(insert, email)

	id := repo.GetIdByEmail(email)

	if id == 0 {
		t.Error("Email should exist")
	}

}

func TestGetIdByPasswordResetToken(t *testing.T) {

	repo := NewUsersRepository()

	clearDB()

	token := "token"

	insert := `INSERT INTO users (password_reset_token) VALUES (?)`

	db.MustExec(insert, token)

	id := repo.GetIdByPasswordResetToken(token)

	if id == 0 {
		t.Error("Email should exist")
	}

}
