-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE users ADD address varchar(255);
ALTER TABLE users ADD name varchar(255);
ALTER TABLE users ADD tel varchar(255);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE users DROP column address;
ALTER TABLE users DROP column name;
ALTER TABLE users DROP column tel;

