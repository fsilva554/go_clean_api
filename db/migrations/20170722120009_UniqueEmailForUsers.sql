-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE users ADD CONSTRAINT uc_email UNIQUE(email);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE users DROP INDEX uc_email;
