-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE users ADD from_google boolean;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE users DROP COLUMN from_google;
