package routes

import (
	"fmt"
	"fsilva-api/services"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type GoogleSignInRoute struct {
	Service services.IGoogleSignInService
}

func NewGoogleSignInRoute() GoogleSignInRoute {
	route := GoogleSignInRoute{}
	route.Service = services.NewGoogleSignInService()
	return route
}

func (route GoogleSignInRoute) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	email := r.FormValue("email")

	token, err := route.Service.Run(email)

	if err != nil {
		fmt.Fprint(w, `{"error": "`+err.Error()+`"}`)
		return
	}

	fmt.Fprint(w, `{"token": "`+token+`"}`)

}
