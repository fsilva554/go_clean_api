package routes

import (
	"encoding/json"
	"fmt"
	"fsilva-api/services"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type GetProfileRoute struct {
	Service services.IGetProfileService
}

func NewGetProfileRoute() GetProfileRoute {
	route := GetProfileRoute{}
	route.Service = services.NewGetProfileService()
	return route
}

func (route GetProfileRoute) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	token := r.Header["X-Auth"]

	if len(token) == 0 {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	profile, err := route.Service.Run(token[0])

	if err != nil {
		fmt.Fprint(w, `{"error": "`+err.Error()+`"}`)
		return
	}

	result, _ := json.Marshal(profile)
	fmt.Fprint(w, string(result))
}
