package routes

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

type MockEditProfileService struct {
	ValidationError bool
}

func (mock *MockEditProfileService) Run(token, email, name, address, tel string) error {
	if mock.ValidationError {
		return errors.New("Wrong username/password")
	}
	return nil
}

func TestEditProfileOK(t *testing.T) {

	route := NewEditProfileRoute()
	route.Service = &MockEditProfileService{}

	form := url.Values{
		"email":    {"a@a.com"},
		"address":  {"Marco Bruto"},
		"name":     {"Fabian"},
		"tel":      {"111"},
		"password": {"aaaaaaaa"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("PUT", "/profile", body)

	token := "token"
	req.Header.Set("X-Auth", token)

	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

}
