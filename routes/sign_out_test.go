package routes

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

type MockSignOutService struct {
}

func (mock *MockSignOutService) Run(token string) {
}

func TestSignOutOK(t *testing.T) {

	route := NewSignOutRoute()
	route.Service = &MockSignOutService{}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/sign_out", nil)

	token := "token"
	req.Header.Set("X-Auth", token)

	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

}
