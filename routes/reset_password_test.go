package routes

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

type MockResetPasswordService struct {
	ValidationError bool
}

func (mock *MockResetPasswordService) Run(email, password string) error {
	if mock.ValidationError {
		return errors.New("Wrong username/password")
	}
	return nil
}

func TestResetPasswordOK(t *testing.T) {

	route := NewResetPasswordRoute()
	route.Service = &MockResetPasswordService{}

	form := url.Values{
		"token":    {"token"},
		"password": {"aaaaaaaa"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/reset_password", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

}
