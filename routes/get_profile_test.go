package routes

import (
	"encoding/json"
	"errors"
	"fsilva-api/structs"
	"net/http"
	"net/http/httptest"
	"testing"
)

type MockGetProfileService struct {
	ValidationError bool
}

func (mock *MockGetProfileService) Run(token string) (structs.Profile, error) {
	if mock.ValidationError {
		return structs.Profile{}, errors.New("Wrong username/password")
	}

	profile := structs.Profile{
		Email:   "a@a.com",
		Name:    "Fabian",
		Address: "Marco bruto",
		Tel:     "111"}

	return profile, nil
}

func TestGetProfileOK(t *testing.T) {

	route := NewGetProfileRoute()
	route.Service = &MockGetProfileService{}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("GET", "/profile", nil)

	token := "token"
	req.Header.Set("X-Auth", token)

	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

	profile := structs.Profile{
		Email:   "a@a.com",
		Name:    "Fabian",
		Address: "Marco bruto",
		Tel:     "111"}

	result, _ := json.Marshal(profile)

	expected := string(result)
	if rr.Body.String() != expected {
		t.Errorf("Unexpected body: got %v, want %v",
			rr.Body.String(), expected)
	}

}

func TestGetProfileValidationError(t *testing.T) {

	route := NewGetProfileRoute()

	mockService := &MockGetProfileService{}
	mockService.ValidationError = true

	route.Service = mockService

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("GET", "/profile", nil)

	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

}
