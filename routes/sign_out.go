package routes

import (
	"fsilva-api/services"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type SignOutRoute struct {
	Service services.ISignOutService
}

func NewSignOutRoute() SignOutRoute {
	route := SignOutRoute{}
	route.Service = services.NewSignOutService()
	return route
}

func (route SignOutRoute) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	token := r.Header["X-Auth"]

	if len(token) == 0 {
		return
	}

	route.Service.Run(token[0])

}
