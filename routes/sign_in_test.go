package routes

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

type MockSignInService struct {
	ValidationError bool
}

func (mock *MockSignInService) Run(email, password string) (string, error) {
	if mock.ValidationError {
		return "", errors.New("Wrong username/password")
	}
	return "token", nil
}

func TestSignInOK(t *testing.T) {

	route := NewSignInRoute()
	route.Service = &MockSignInService{}

	form := url.Values{
		"email":    {"a@a.com"},
		"password": {"aaaaaaaa"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/sign_in", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

	expected := `{"token": "token"}`
	if rr.Body.String() != expected {
		t.Errorf("Unexpected body: got %v, want %v",
			rr.Body.String(), expected)
	}

}

func TestSignInValidationError(t *testing.T) {

	route := NewSignInRoute()

	mockService := &MockSignInService{}
	mockService.ValidationError = true

	route.Service = mockService

	form := url.Values{
		"email":    {"a@a.com"},
		"password": {"aaaaaaaa"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/sign_in", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

	expected := `{"error": "Wrong username/password"}`
	if rr.Body.String() != expected {
		t.Errorf("Unexpected body: got %v, want %v",
			rr.Body.String(), expected)
	}

}
