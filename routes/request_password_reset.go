package routes

import (
	"fmt"
	"fsilva-api/services"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type RequestPasswordResetRoute struct {
	Service services.IRequestPasswordResetService
}

func NewRequestPasswordResetRoute() RequestPasswordResetRoute {
	route := RequestPasswordResetRoute{}
	route.Service = services.NewRequestPasswordResetService()
	return route
}

func (route RequestPasswordResetRoute) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	email := r.FormValue("email")

	err := route.Service.Run(email)

	if err != nil {
		fmt.Fprint(w, `{"error": "`+err.Error()+`"}`)
		return
	}

}
