package routes

import (
	"fmt"
	"fsilva-api/services"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type SignInRoute struct {
	Service services.ISignInService
}

func NewSignInRoute() SignInRoute {
	route := SignInRoute{}
	route.Service = services.NewSignInService()
	return route
}

func (route SignInRoute) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	email := r.FormValue("email")
	password := r.FormValue("password")

	token, err := route.Service.Run(email, password)

	if err != nil {
		fmt.Fprint(w, `{"error": "`+err.Error()+`"}`)
		return
	}

	fmt.Fprint(w, `{"token": "`+token+`"}`)

}
