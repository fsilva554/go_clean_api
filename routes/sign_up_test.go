package routes

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

type MockSignUpService struct {
	ValidationError bool
}

func (mock *MockSignUpService) Run(email, password string) (string, error) {
	if mock.ValidationError {
		return "", errors.New("Wrong username/password")
	}
	return "token", nil
}

func TestSignUpOK(t *testing.T) {

	route := NewSignUpRoute()
	route.Service = &MockSignUpService{}

	form := url.Values{
		"email":    {"a@a.com"},
		"password": {"aaaaaaaa"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/sign_up", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

	expected := `{"token": "token"}`
	if rr.Body.String() != expected {
		t.Errorf("Unexpected body: got %v, want %v",
			rr.Body.String(), expected)
	}

}

func TestSignUpValidationError(t *testing.T) {

	route := NewSignUpRoute()

	mockService := &MockSignUpService{}
	mockService.ValidationError = true

	route.Service = mockService

	form := url.Values{
		"email":    {"a@a.com"},
		"password": {"aaaaaaaa"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/sign_up", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

	expected := `{"error": "Wrong username/password"}`
	if rr.Body.String() != expected {
		t.Errorf("Unexpected body: got %v, want %v",
			rr.Body.String(), expected)
	}

}
