package routes

import (
	"fmt"
	"fsilva-api/services"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type ResetPasswordRoute struct {
	Service services.IResetPasswordService
}

func NewResetPasswordRoute() ResetPasswordRoute {
	route := ResetPasswordRoute{}
	route.Service = services.NewResetPasswordService()
	return route
}

func (route ResetPasswordRoute) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	token := r.FormValue("token")
	password := r.FormValue("password")

	err := route.Service.Run(token, password)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, `{"error": "`+err.Error()+`"}`)
		return
	}

}
