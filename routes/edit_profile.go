package routes

import (
	"fmt"
	"fsilva-api/services"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type EditProfileRoute struct {
	Service services.IEditProfileService
}

func NewEditProfileRoute() EditProfileRoute {
	route := EditProfileRoute{}
	route.Service = services.NewEditProfileService()
	return route
}

func (route EditProfileRoute) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	token := r.Header["X-Auth"]

	if len(token) == 0 {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	r.ParseForm()

	email := r.FormValue("email")
	name := r.FormValue("name")
	address := r.FormValue("address")
	tel := r.FormValue("tel")

	err := route.Service.Run(token[0], email, name, address, tel)

	if err != nil {
		fmt.Fprint(w, `{"error": "`+err.Error()+`"}`)
		return
	}
}
