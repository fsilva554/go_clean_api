package routes

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

type MockGoogleSignInService struct {
	ValidationError bool
}

func (mock *MockGoogleSignInService) Run(email string) (string, error) {
	if mock.ValidationError {
		return "", errors.New("Email exists")
	}
	return "token", nil
}

func TestGoogleSignInOK(t *testing.T) {

	route := NewGoogleSignInRoute()
	route.Service = &MockGoogleSignInService{}

	form := url.Values{
		"email": {"a@a.com"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/google_sign_in", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

	expected := `{"token": "token"}`
	if rr.Body.String() != expected {
		t.Errorf("Unexpected body: got %v, want %v",
			rr.Body.String(), expected)
	}

}

func TestGoogleSignInValidationError(t *testing.T) {

	route := NewSignInRoute()

	mockService := &MockSignInService{}
	mockService.ValidationError = true

	route.Service = mockService

	form := url.Values{
		"email": {"a@a.com"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/sign_in", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

	expected := `{"error": "Wrong username/password"}`
	if rr.Body.String() != expected {
		t.Errorf("Unexpected body: got %v, want %v",
			rr.Body.String(), expected)
	}

}
