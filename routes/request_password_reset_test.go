package routes

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

type MockRequestPasswordResetService struct {
}

func (mock *MockRequestPasswordResetService) Run(email string) error {
	return nil
}

func TestRequestPasswordResetOK(t *testing.T) {

	route := NewGoogleSignInRoute()
	route.Service = &MockGoogleSignInService{}

	form := url.Values{
		"email": {"a@a.com"},
	}

	body := bytes.NewBufferString(form.Encode())

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { route.Run(w, r, nil) })

	req, err := http.NewRequest("POST", "/request_password_reset", body)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v, want %v",
			status, http.StatusOK)
	}

}
