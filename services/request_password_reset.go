package services

import (
	"errors"
	"fsilva-api/db"
	"fsilva-api/thirdparty"
	"fsilva-api/utils"
)

type IRequestPasswordResetService interface {
	Run(string) error
}

type RequestPasswordResetService struct {
	UsersRepository db.IUsersRepository
	EmailSender     thirdparty.IEmailSender
}

func NewRequestPasswordResetService() RequestPasswordResetService {
	service := RequestPasswordResetService{}
	service.UsersRepository = db.NewUsersRepository()
	service.EmailSender = thirdparty.EmailSender{}
	return service
}

func (service RequestPasswordResetService) Run(email string) error {

	id := service.UsersRepository.GetIdByEmail(email)

	if id == 0 {
		return errors.New("Email doesn't exist")
	}

	token := utils.GenerateSessionToken(email, "randomPasswordText")

	service.UsersRepository.StorePasswordResetToken(id, token)

	service.EmailSender.SendResetPassword(email, token)

	return nil
}
