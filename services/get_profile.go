package services

import (
	"errors"
	"fsilva-api/db"
	"fsilva-api/structs"
)

type IGetProfileService interface {
	Run(string) (structs.Profile, error)
}

type GetProfileService struct {
	UsersRepository db.IUsersRepository
}

func NewGetProfileService() GetProfileService {
	service := GetProfileService{}
	service.UsersRepository = db.NewUsersRepository()
	return service
}

func (service GetProfileService) Run(token string) (structs.Profile, error) {

	profile := service.UsersRepository.GetProfileBySessionToken(token)

	if profile.Email == "" {
		return profile, errors.New("No profile found")
	}

	return profile, nil
}
