package services

import (
	"errors"
	"fsilva-api/db"
	"fsilva-api/utils"
	"strings"
)

type IGoogleSignInService interface {
	Run(string) (string, error)
}

type GoogleSignInService struct {
	UsersRepository db.IUsersRepository
}

func NewGoogleSignInService() GoogleSignInService {
	service := GoogleSignInService{}
	service.UsersRepository = db.NewUsersRepository()
	return service
}

func (service GoogleSignInService) Run(email string) (string, error) {

	id := service.UsersRepository.GetIdByEmailFromGoogle(email)

	if id == 0 {
		newId, err := service.UsersRepository.CreateGoogleSignup(email)
		if err != nil {
			if strings.Contains(err.Error(), "Duplicate") {
				return "", errors.New("Account is not linked to google")
			} else {
				return "", err
			}
		}
		id = newId
	}

	token := utils.GenerateSessionToken(email, "randomText")

	service.UsersRepository.StoreSessionToken(id, token)

	return token, nil
}
