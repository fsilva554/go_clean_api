package services

import (
	"errors"
	"fsilva-api/db"
	"fsilva-api/utils"
	"log"
)

type ISignInService interface {
	Run(string, string) (string, error)
}

type SignInService struct {
	UsersRepository db.IUsersRepository
}

func NewSignInService() SignInService {
	service := SignInService{}
	service.UsersRepository = db.NewUsersRepository()
	return service
}

func (service SignInService) Run(email, password string) (string, error) {
	hashedPassword := utils.Encrypt(password)

	id := service.UsersRepository.GetIdByEmailAndPassword(email, hashedPassword)

	if id == 0 {
		return "", errors.New("Wrong email/password")
	}

	token := utils.GenerateSessionToken(email, password)

	service.UsersRepository.StoreSessionToken(id, token)

	log.Print("Here we go")

	return token, nil
}
