package services

import (
	"fsilva-api/db/mocks"
	"testing"
)

func TestSignOutServiceOK(t *testing.T) {

	service := NewSignOutService()

	repo := &mocks.MockUsersRepository{}
	service.UsersRepository = repo

	token := "token"

	service.Run(token)

	if !repo.DeleteSessionWasCalled {
		t.Error("Delete session should be called")
	}

}
