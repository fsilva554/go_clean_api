package services

import (
	"fsilva-api/db/mocks"
	"testing"
)

func TestGoogleSignInServiceOK(t *testing.T) {

	service := NewGoogleSignInService()

	repo := &mocks.MockUsersRepository{GetIdByEmailFromGoogleReturnResults: true}
	service.UsersRepository = repo

	email := "a@a.com"

	result, err := service.Run(email)

	if !repo.GetIdByEmailFromGoogleWasCalled {
		t.Error("Email check should be called")
	}

	if !repo.StoreSessionTokenWasCalled {
		t.Error("Store session token should be called")
	}

	if err != nil {
		t.Error("No error should be returned")
	}

	if result == "" {
		t.Error("Token should be returned")
	}
}

func TestGoogleSignInServiceCreateSignup(t *testing.T) {

	service := NewGoogleSignInService()

	repo := &mocks.MockUsersRepository{GetIdByEmailFromGoogleReturnResults: false}
	service.UsersRepository = repo

	email := "a@a.com"

	result, err := service.Run(email)

	if !repo.GetIdByEmailFromGoogleWasCalled {
		t.Error("Email check should be called")
	}

	if !repo.CreateGoogleSignupWasCalled {
		t.Error("Create google signup should be called")
	}

	if !repo.StoreSessionTokenWasCalled {
		t.Error("Store session token should be called")
	}

	if err != nil {
		t.Error("No error should be returned")
	}

	if result == "" {
		t.Error("Token should be returned")
	}
}
