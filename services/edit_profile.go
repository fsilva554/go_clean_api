package services

import (
	"errors"
	"fsilva-api/db"
	"strings"
)

type IEditProfileService interface {
	Run(string, string, string, string, string) error
}

type EditProfileService struct {
	UsersRepository db.IUsersRepository
}

func NewEditProfileService() EditProfileService {
	service := EditProfileService{}
	service.UsersRepository = db.NewUsersRepository()
	return service
}

func (service EditProfileService) Run(token, email, name, address, tel string) error {

	err := service.UsersRepository.UpdateProfileBySessionToken(token, email, name, address, tel)

	if err != nil {
		if strings.Contains(err.Error(), "Duplicate") {
			return errors.New("Email is taken")
		} else {
			return err
		}
	}

	return nil
}
