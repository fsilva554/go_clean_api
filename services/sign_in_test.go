package services

import (
	"fsilva-api/db/mocks"
	"testing"
)

func TestSignInServiceOK(t *testing.T) {

	service := NewSignInService()

	repo := &mocks.MockUsersRepository{GetIdByEmailAndPasswordReturnResults: true}
	service.UsersRepository = repo

	email := "a@a.com"
	password := "aaaaaaaa"

	result, err := service.Run(email, password)

	if !repo.GetIdByEmailAndPasswordWasCalled {
		t.Error("Email and password check should be called")
	}

	if !repo.StoreSessionTokenWasCalled {
		t.Error("Store session token should be called")
	}

	if err != nil {
		t.Error("No error should be returned")
	}

	if result == "" {
		t.Error("Token should be returned")
	}
}
