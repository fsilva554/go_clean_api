package services

import (
	"errors"
	"fsilva-api/db"
	"fsilva-api/utils"
)

type ISignUpService interface {
	Run(string, string) (string, error)
}

type SignUpService struct {
	UsersRepository db.IUsersRepository
}

func NewSignUpService() SignUpService {
	service := SignUpService{}
	service.UsersRepository = db.NewUsersRepository()
	return service
}

func (service SignUpService) Run(email, password string) (string, error) {
	hashedPassword := utils.Encrypt(password)

	id := service.UsersRepository.CreateUser(email, hashedPassword)

	if id == 0 {
		return "", errors.New("Email is taken")
	}

	token := utils.GenerateSessionToken(email, password)

	service.UsersRepository.StoreSessionToken(id, token)

	return token, nil
}
