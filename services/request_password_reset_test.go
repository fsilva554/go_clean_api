package services

import (
	"fsilva-api/db/mocks"
	"testing"
)

type MockEmailSender struct{}

func (mock MockEmailSender) SendResetPassword(email, token string) {

}

func TestRequestPasswordResetServiceOK(t *testing.T) {

	service := NewRequestPasswordResetService()

	repo := &mocks.MockUsersRepository{GetIdByEmailReturnResults: true}
	service.UsersRepository = repo
	service.EmailSender = &MockEmailSender{}

	email := "w.fabian.silva@gmail.com"

	err := service.Run(email)

	if !repo.GetIdByEmailWasCalled {
		t.Error("Get id by email should be called")
	}

	if !repo.StorePasswordResetTokenWasCalled {
		t.Error("Store password reset token should be called")
	}

	if err != nil {
		t.Error("No error should be returned")
	}
}
