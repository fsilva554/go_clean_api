package services

import (
	"fsilva-api/db"
)

type ISignOutService interface {
	Run(string)
}

type SignOutService struct {
	UsersRepository db.IUsersRepository
}

func NewSignOutService() SignOutService {
	service := SignOutService{}
	service.UsersRepository = db.NewUsersRepository()
	return service
}

func (service SignOutService) Run(token string) {
	service.UsersRepository.DeleteSessionToken(token)
}
