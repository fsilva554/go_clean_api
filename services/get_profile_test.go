package services

import (
	"fsilva-api/db/mocks"
	"testing"
)

func TestGetProfileServiceOK(t *testing.T) {

	service := NewGetProfileService()

	repo := &mocks.MockUsersRepository{GetProfileBySessionTokenReturnResults: true}
	service.UsersRepository = repo

	result, err := service.Run("token")

	if !repo.GetProfileBySessionTokenWasCalled {
		t.Error("Get profile should be called")
	}

	if err != nil {
		t.Error(err)
		t.Error("No error should be returned")
	}

	if result.Email == "" {
		t.Error("Profile should be returned")
	}
}
