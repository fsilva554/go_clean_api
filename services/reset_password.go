package services

import (
	"errors"
	"fsilva-api/db"
	"fsilva-api/utils"
)

type IResetPasswordService interface {
	Run(string, string) error
}

type ResetPasswordService struct {
	UsersRepository db.IUsersRepository
}

func NewResetPasswordService() ResetPasswordService {
	service := ResetPasswordService{}
	service.UsersRepository = db.NewUsersRepository()
	return service
}

func (service ResetPasswordService) Run(token, password string) error {

	id := service.UsersRepository.GetIdByPasswordResetToken(token)

	if id == 0 {
		return errors.New("Wrong token")
	}

	hashedPassword := utils.Encrypt(password)

	service.UsersRepository.UpdatePassword(id, hashedPassword)

	return nil
}
