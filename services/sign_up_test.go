package services

import (
	"fsilva-api/db/mocks"
	"testing"
)

func TestSignUpServiceOK(t *testing.T) {

	service := NewSignUpService()

	repo := &mocks.MockUsersRepository{CreateUserDuplicateEmail: false}
	service.UsersRepository = repo

	email := "a@a.com"
	password := "aaaaaaaa"

	result, err := service.Run(email, password)

	if !repo.CreateUserWasCalled {
		t.Error("Create user should be called")
	}

	if !repo.StoreSessionTokenWasCalled {
		t.Error("Store session token should be called")
	}

	if err != nil {
		t.Error("No error should be returned")
	}

	if result == "" {
		t.Error("Token should be returned")
	}
}

func TestSignUpServiceDuplicateEmail(t *testing.T) {

	service := NewSignUpService()

	repo := &mocks.MockUsersRepository{CreateUserDuplicateEmail: true}
	service.UsersRepository = repo

	email := "a@a.com"
	password := "aaaaaaaa"

	_, err := service.Run(email, password)

	if !repo.CreateUserWasCalled {
		t.Error("Create user should be called")
	}

	if err == nil {
		t.Error("Error should be returned")
	}
}
