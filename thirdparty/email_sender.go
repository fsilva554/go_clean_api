package thirdparty

import (
	"fmt"
	"log"

	sendgrid "github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type IEmailSender interface {
	SendResetPassword(string, string)
}

type EmailSender struct {
}

func (e EmailSender) SendResetPassword(email, token string) {
	from := mail.NewEmail("Fabian", "w.fabian.silva@gmail.com")
	subject := "Sending with SendGrid is Fun"
	to := mail.NewEmail(email, email)
	plainContent := "A password reset has been requested. Please click the link to conitnue."
	link := "https://fsilva-fe.herokuapp.com/reset_password?token=" + token
	//htmlContent := "<a href='Reset password</a>"
	htmlContent := "<p>Hello there!</p><p>" + plainContent + "</p><a href=" + link + ">Reset password</a>"
	message := mail.NewSingleEmail(from, subject, to, plainContent, htmlContent)

	client := sendgrid.NewSendClient("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")

	response, err := client.Send(message)
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
	}
}
